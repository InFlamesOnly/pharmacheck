//
//  AppDelegate.swift
//  PharmaCheck
//
//  Created by macOS on 30.07.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseCore
import Firebase
import FirebaseMessaging
import UserNotifications
import CoreLocation


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var locationManager = CLLocationManager()
    var bgTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier.invalid
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.startApp(application: application)
        return true
    }
    
    private func startApp (application: UIApplication) {
        self.registreNotificationToApp(application: application)
        self.getUserLocation()
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
    }
    
    private func getUserLocation () {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    //MARK : - NOTIFICATION
    func registreNotificationToApp(application : UIApplication) {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [UNAuthorizationOptions.sound ], completionHandler: { (granted, error) in
                if granted {
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                }
                //                if error == nil{
                //                    UIApplication.shared.registerForRemoteNotifications()
                //                }
            })
        } else {
            //            let settings  = UIUserNotificationSettings(types: [UIUserNotificationType.alert , UIUserNotificationType.badge , UIUserNotificationType.sound], categories: nil)
            //            UIApplication.shared.registerUserNotificationSettings(settings)
            //            application.registerForRemoteNotifications()
        }
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info === \(notification.request.content.userInfo)")
        //        if let notInfo = notification.request.content.userInfo as? NSDictionary {
        //            if let imageUrl = notInfo.value(forKey: "image_url") as? String {
        //                let text = notInfo.value(forKey: "text") as! String
        //                self.addHolidayFromServerRespons(imageUrl: imageUrl, text: text)
        //            }
        //        }
        //
        //        if let aps = notification.request.content.userInfo["aps"] as? NSDictionary {
        //            if let alert = aps["alert"] as? NSDictionary {
        //                if let body = alert["body"] as? NSString {
        //                    if body == "Гороскоп на лето" {
        //                        UserDefaults.standard.set(true, forKey: "holiday")
        //                    }
        //                }
        //            }
        //        }
        // Handle code here.
        completionHandler([UNNotificationPresentationOptions.sound , UNNotificationPresentationOptions.alert , UNNotificationPresentationOptions.badge])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("User Info === \(response.notification.request.content.userInfo)")
        //        if let notInfo = response.notification.request.content.userInfo as? NSDictionary {
        //            if let imageUrl = notInfo.value(forKey: "image_url") as? String {
        //                let text = notInfo.value(forKey: "text") as! String
        //                let state: UIApplicationState = UIApplication.shared.applicationState // or use  let state =  UIApplication.sharedApplication().applicationState
        //
        //                if state == .inactive {
        //                    _ = Timer.scheduledTimer(withTimeInterval: 1.6, repeats: false) { (timer) in
        //                        self.addHolidayFromServerRespons(imageUrl: imageUrl, text: text)
        //
        //                    }
        //                }
        //                else {
        //                    self.addHolidayFromServerRespons(imageUrl: imageUrl, text: text)
        //                }
        //            }
        //
        //        }
        //        if let aps = response.notification.request.content.userInfo["aps"] as? NSDictionary {
        //            if let alert = aps["alert"] as? NSDictionary {
        //                if let body = alert["body"] as? NSString {
        //                    if body == "Гороскоп на лето" {
        //                        UserDefaults.standard.set(true, forKey: "holiday")
        //                    }
        //                }
        //            }
        //        }
        completionHandler()
    }
    
    ///////////////////////
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (timer) in
            let tokenString = deviceToken.reduce("", {$0 + String(format: "%02X",    $1)})
            UserDefaults.standard.set(tokenString, forKey: "deviceId")
            let token = Messaging.messaging().fcmToken
            self.send(idDevice: tokenString, firebaseId: token!)
            if User.userIsFind() == true {
                let user = User.currentUser()
                user.updateFCMToken(firebaseToken: Messaging.messaging().fcmToken!)
                user.updateDeviceToken(deviceToken: tokenString)
            }
            print(tokenString)
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    func send (idDevice : String, firebaseId : String) {
        RequestManager.shared.setDevice(deviceToken: idDevice, fcmToken: firebaseId, success: { (responseObject) in
            print("\(responseObject)")
        }) { (errorCode) in
            
        }
    }
    
    func update (fcm : String) {
        RequestManager.shared.setFCM(fcmToken: fcm, success: { (responseObject) in
            print("\(responseObject)")
        }) { (_) in
            
        }
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
        let token = String(data: deviceToken.base64EncodedData(), encoding: .utf8)?.trimmingCharacters(in: CharacterSet.whitespaces).trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        print("\(token ?? "")")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        if let token = Messaging.messaging().fcmToken {
            if User.userIsFind() == true {
                let user = User.currentUser()
                user.updateFCMToken(firebaseToken: token)
                self.update(fcm: token)
            }
            print("FCM token: \(token)")
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        UIApplication.shared.registerForRemoteNotifications()
        self.bgTask = application.beginBackgroundTask(expirationHandler: {
            application.endBackgroundTask(self.bgTask)
            self.bgTask = UIBackgroundTaskIdentifier.invalid
        })
        
        DispatchQueue.global(qos: .default).async(execute: {
            if User.userIsFind() == true {
                RequestManager.shared.offline(success: { (response) in
                    application.endBackgroundTask(self.bgTask)
                    self.bgTask = UIBackgroundTaskIdentifier.invalid
                }, failure: { (error) in
                    
                })
            }
        })
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        UIApplication.shared.registerForRemoteNotifications()
        self.bgTask = application.beginBackgroundTask(expirationHandler: {
            application.endBackgroundTask(self.bgTask)
            self.bgTask = UIBackgroundTaskIdentifier.invalid
        })
        
        DispatchQueue.global(qos: .default).async(execute: {
            if User.userIsFind() == true {
                RequestManager.shared.offline(success: { (response) in
                    application.endBackgroundTask(self.bgTask)
                    self.bgTask = UIBackgroundTaskIdentifier.invalid
                }, failure: { (error) in
                    
                })
            }
        })
    }
    
    
}

extension AppDelegate : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
    }
}

extension AppDelegate : MessagingDelegate {
    
}

