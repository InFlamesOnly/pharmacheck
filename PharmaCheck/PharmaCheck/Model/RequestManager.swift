//
//  RequestManager.swift
//  Remedy
//
//  Created by macOS on 05.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import Alamofire

private enum Errors : String {
    case Password = "Неверный пароль!"
    case User = "Такого юзера не существует"
    case Code = "Неправильный код подтверждения!"
}

private let headers = ["Content-type": "application/x-www-form-urlencoded" , "Accept" : "application/json"]

private let kAuth = "auth"
private let kCode = "gencode"
private let kCheckCode = "checkcode"
private let kNewPassword = "resetpassword"
private let kAllResearch = "getalllists"
private let kWorksheets = "getmyworksheets"
private let kMyProfile = "getmyprofile"
private let kAcounts = "getmyauth"
private let kRegDevice = "reg_device"
private let kUserFromToken = "token_auth"
private let kFCMToken = "setFCMToken"
private let kClearDeviceId = "clear_nonauth"
private let kContent = "get_content"
private let kPlusView = "plus_view"
// let request = String(format:"%@?token=%@",kCalendarDate,user.token)
private let kCalendarDate = "get_all_calendar"

//ONLINE OFFLINE
private let kOnline = "online"
private let kOffline = "offline"


class RequestManager: NSObject {
    
    static let shared = RequestManager()
    
}

extension RequestManager {
    
    func getHistoryList (success: @escaping (_ text : String) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let url = "\(kAPIServer)history_list"
        let parameters = ["user":["token":User.currentUser().token]]
        self.post(request: url, parameters: parameters, success: { succes in
            
            if let user = succes["user"] as? [String:Any] {
                success(user["html"] as! String)
            }
            
        }) { error in
            
        }
    }
    
    func getHistoryBonus (success: @escaping (_ text : String) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let url = "\(kAPIServer)history_bonus"
        let parameters = ["user":["token":User.currentUser().token]]
        self.post(request: url, parameters: parameters, success: { succes in
            
            if let user = succes["user"] as? [String:Any] {
                success(user["html"] as! String)
            }
            
        }) { error in
            
        }
    }

    func getUserWorkSheetsWithBonusessText (success: @escaping (_ text : String, _ companyId : Int, _ bonuesURL : String, _ howUseBonuesURL : String, _ sucessBlock : [Form]) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let url = "\(kAPIServer)getmyworksheets"
        //BcnyBPG2BYwmCRCoRYkwiJRn5x1ePx2w6TEnxwRvTc0JcnSUb
//        let parameters = ["user":["token":"BcnyBPG2BYwmCRCoRYkwiJRn5x1ePx2w6TEnxwRvTc0JcnSUb"]]
        let parameters = ["user":["token":User.currentUser().token]]
        self.post(request: url, parameters: parameters, success: { succes in
            
            if let user = succes["user"] as? [String:Any] {
                if let user_data = user["user_data"] as? [String:Any] {
                    success(user_data["text"] as! String,
                            user["compnay_id"] as! Int,
                            user_data["bonus_url"] as! String,
                            user_data["how_use_bonus"] as! String,
                            Form.mapResponseToArrayObject(response: succes))
                }
            }
            
        }) { error in
            
        }
    }
    
    func calendarDates (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = "\(kAPIServer)\(kCalendarDate)?token=\(User.currentUser().token)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }

    func offline (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = "https://chat.remedy-ua.com/index.php?act=leave_chat&token=\(User.currentUser().token)"
        //        let request = "\(kAPIServer)\(kOffline)?token=\(User.currentUser().token)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func auth (phone : String, password : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["phone" : phone,
                                    "password" : password]]
        let request = "\(kAPIServer)\(kAuth)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func plusContent (article : Article, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["token" : User.currentUser().token,
                          "post_id" : article.id] as [String : Any]
        let request = "\(kAPIServer)\(kPlusView)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func content (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = "\(kAPIServer)\(kContent)?token=\(User.currentUser().token)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func forgotPassword (phone : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["phone" : phone]]
        let request = "\(kAPIServer)\(kCode)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func checkCode (phone : String, code : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["phone" : phone, "code" : code]]
        let request = "\(kAPIServer)\(kCheckCode)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func newPassword (phone : String, code : String, newPassword : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["phone" : phone, "code" : code, "new_password" : newPassword]]
        let request = "\(kAPIServer)\(kNewPassword)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
}

extension RequestManager {
    func userFromToken (token : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : token]]
        let request = "\(kAPIServer)\(kUserFromToken)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func researchList (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : User.currentUser().token]]
        let request = "\(kAPIServer)\(kAllResearch)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func worksheetsInfo (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void,
                         bonusesText: @escaping (_ bonusesText: String) -> Void,
                         failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : User.currentUser().token]]
        let request = "\(kAPIServer)\(kWorksheets)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            bonusesText(self.bonusesText(responseObject: responseObject))
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func bonusesText (responseObject: Dictionary<String, Any>) -> String {
        var bonusesText = ""
        if let user = responseObject["user"] as? [String: Any] {
            if let userData = user["user_data"] as? [String: Any] {
                let text = userData["text"] as? String ?? ""
                bonusesText = text
            }
        }
        return bonusesText
    }
    
    func myProfile (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void,
                    failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : User.currentUser().token]]
        let request = "\(kAPIServer)\(kMyProfile)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func setDevice (deviceToken : String, fcmToken : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["device_id" : deviceToken,
                                    "fcm_token" : fcmToken,
                                    "device" : "IOS"]]
        let request = "\(kAPIServer)\(kRegDevice)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func setFCM (fcmToken : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["token" : User.currentUser().token, "fcmtoken" : fcmToken]]
        let request = "\(kAPIServer)\(kFCMToken)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func clearDeviceId (deviceId : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["user" : ["device_id" : deviceId]]
        let request = "\(kAPIServer)\(kClearDeviceId)"
        self.post(request: request, parameters: parameters, success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
    
    func acoounts (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void,
                    failure: @escaping (_ error: Int?) -> Void) {
//        let parameters = ["user" : ["token" : User.currentUser().token]]
//        NSString *request = [NSString stringWithFormat:@"%@?token=%@",getUsers,user.token];
        let request = "\(kAPIServer)\(kAcounts)?token=\(User.currentUser().token)"
        self.get(request: request, parameters: [:], success: { (responseObject) in
            success(responseObject)
        }) { (errorCode) in
            self.error(code: errorCode!)
            failure(errorCode)
        }
    }
}

extension RequestManager {
    private func error (code : Int) {
        switch code  {
        case 201:
            self.showError(message: Errors.Password.rawValue)
            break
        case 202:
            self.showError(message: Errors.User.rawValue)
            break
        case 203:
            self.showError(message: Errors.Code.rawValue)
            break
        default:
            break
        }
    }
    
    private func showError (message : Errors.RawValue) {
        if let topController = UIApplication.topViewController() {
            topController.presentAlert(withTitle: "Ошибка", message: message)
        }
    }
}

extension RequestManager {
    private func get (request : String , parameters: [AnyHashable: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == 200 {
                    success(response.result.value! as! Dictionary<String, Any>)
                } else {
                    failure(response.response?.statusCode)
                }
                print(response.result.value!)
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result.error!)
                break
            }
        }
    }
    
    private func delete (request : String , parameters: [String: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        Alamofire.request(request, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == 200 {
                    success(response.result.value! as! Dictionary<String, Any>)
                } else {
                    failure(response.response?.statusCode)
                }
                print(response.result.value!)
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result.error!)
                break
            }
        }
    }
    
    private func put (request : String , parameters: [String: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        Alamofire.request(request, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == 200 {
                    success(response.result.value! as! Dictionary<String, Any>)
                } else {
                    failure(response.response?.statusCode)
                }
                print(response.result.value!)
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result.error!)
                break
            }
        }
    }
    
    private func post (request : String , parameters: [String: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == 200 {
                    success(response.result.value! as! Dictionary<String, Any>)
                } else {
                    failure(response.response?.statusCode)
                }
                print(response.result.value!)
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result.error!)
                break
            }
        }
    }
}
