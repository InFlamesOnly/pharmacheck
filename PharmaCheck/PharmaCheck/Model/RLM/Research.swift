//
//  Research.swift
//  Remedy
//
//  Created by macOS on 06.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Research: Object {
    @objc dynamic var isEnableGeo = false
    @objc dynamic var name = ""
    @objc dynamic var id = 0
    @objc dynamic var isNeedShowPaP = false
    @objc dynamic var activeTill = ""
    @objc dynamic var toEnd = ""
    @objc dynamic var policyURL = ""
    @objc dynamic var formURL = ""
    
    func get (response : Dictionary<String, Any>) {
        self.id = response ["id"] as? Int ?? 0
        self.name = response ["name"] as? String ?? ""
        self.isNeedShowPaP = response ["isPrivace"] as? Bool ?? false
        self.activeTill = response ["active_till"] as? String ?? ""
        self.toEnd = response ["to_end"] as? String ?? ""
        self.policyURL = response ["policy_url"] as? String ?? ""
        self.formURL = response ["form_url"] as? String ?? ""
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Research> {
        var array = Array<Research>()
        if let user = response["user"] as? [String: Any] {
            if let arrayObj = user["tasks"] as? [[String:Any]] {
                for arrayDict in arrayObj {
                    let object = Research()
                    let user = (response as [String : AnyObject])["user"]
                    object.isEnableGeo = user! ["geo_enable"] as? Bool ?? false
                    object.get(response: arrayDict)
                    array.append(object)
                    
                }
            }
        }
        return array
    }
}



//self.researchId = [response valueForKey:@"id"];
//self.name = [response valueForKey:@"name"];
//self.activeTill = [response valueForKey:@"active_till"];
//self.toEnd = [response valueForKey:@"to_end"];
//self.policyURL = [response valueForKey:@"policy_url"];
//self.formURL = [response valueForKey:@"form_url"];
//self.isPrivacy = [[response valueForKey:@"isPrivace"] boolValue];
