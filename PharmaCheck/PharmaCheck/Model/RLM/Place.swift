//
//  Place.swift
//  Remedy
//
//  Created by macOS on 11.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Place: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var address = ""
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var status = 0
    
    func get (response : Dictionary<String, Any>) {
        self.id = response ["id"] as? Int ?? 0
        self.name = response ["name"] as? String ?? ""
        self.address = response ["name"] as? String ?? ""
        self.latitude = response ["latitude"] as? Double ?? 0.0
        self.longitude = response ["longitude"] as? Double ?? 0.0
        self.status = response ["status"] as? Int ?? 0
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Place> {
        var array = Array<Place>()
        if let user = response["user"] as? [String: Any] {
            if let userData = user["user_data"] as? [String: Any] {
                if let arrayObj = userData["places"] as? [[String:Any]] {
                    for arrayDict in arrayObj {
                        let object = Place()
                        object.get(response: arrayDict)
                        array.append(object)
                        
                    }
                }
            }
        }
        return array
    }
}
