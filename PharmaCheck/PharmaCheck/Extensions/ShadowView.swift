//
//  ShadowView.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addShadow () {
        self.layer.masksToBounds = false
        self.layer.shadowColor = SHADOW_COLOR.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 8.0)
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 6
    }
}
