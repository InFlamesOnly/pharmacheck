//
//  CalendarDetailVC.swift
//  StarClub
//
//  Created by Hackintosh on 7/5/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class CalendarDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var selectedEvent : CalendarDate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.hideBurger()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.showBurger()
        }
    }
    
    func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale =  NSLocale(localeIdentifier: "ru_RU") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarDetailC", for: indexPath) as! CalendarDetailC
        cell.dateTittle.text = self.selectedEvent?.title
        cell.dateText.text = self.selectedEvent?.fullContent
        let attributedString = NSAttributedString(html: self.selectedEvent?.fullContent ?? "")
        cell.feedTextView.attributedText = attributedString
        cell.feedTextView.font = UIFont(name: "AvenirNext-Medium", size: 15.0)
        cell.dateText.text = cell.feedTextView.text
        cell.feedTextView.isScrollEnabled = false
        if let image = selectedEvent?.image {
            if let url = URL(string: image) {
                cell.dateImageView.af_setImage(withURL: url, placeholderImage: nil)
            }
        }
        let font = UIFont.systemFont(ofSize: 17)
//        let font = UIFont(name: "AvenirNext-Medium", size: 17.0)
        let height = CGFloat().heightForView(text: cell.dateTittle.text!, font: font, width: UIScreen.main.bounds.width) + 40
        cell.heightConstraint.constant = height
        cell.dateImageView.contentMode = .scaleAspectFit
        
        cell.startDateText.text = "Дата начала : \(self.convertDateFormater(self.selectedEvent!.startDate))"
        let height1 = CGFloat().heightForView(text: self.selectedEvent!.startDate, font: font, width: cell.roundView.frame.size.width - 34) + 40
        cell.startDateConstraint.constant = height1
        
        cell.startTimeText.text = "Время начала : \(self.selectedEvent!.startTime)"
        let height2 = CGFloat().heightForView(text: self.selectedEvent!.startTime, font: font, width: cell.roundView.frame.size.width - 34)
        cell.startTimeConstraint.constant = height2
        
        cell.endDateText.text = "Дата конца : \(self.convertDateFormater(self.selectedEvent!.endDate))"
        let height3 = CGFloat().heightForView(text: self.selectedEvent!.endDate, font: font, width: cell.roundView.frame.size.width - 34)  + 40
        cell.endDateConstraint.constant = height3
        
        cell.endTimeText.text = "Время конца : \(self.selectedEvent!.endTime)"
        let height4 = CGFloat().heightForView(text: self.selectedEvent!.endTime, font: font, width: cell.roundView.frame.size.width - 34)
        cell.endTimeConstraint.constant = height4
        
        cell.locationText.text = "Место : \(self.selectedEvent?.location ?? "")"
        let height5 = CGFloat().heightForView(text: self.selectedEvent!.location, font: font, width: cell.roundView.frame.size.width - 34)
        cell.locationTextConstraint.constant = height5
        
        cell.selectionStyle = .none
        return cell
    }
}

extension CGFloat {
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
}
