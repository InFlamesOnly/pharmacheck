//
//  Form.swift
//  PharmaCheck
//
//  Created by macOS on 10/29/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Form: NSObject {
    @objc dynamic var status = 0
    @objc dynamic var text = ""
    @objc dynamic var name = ""
    @objc dynamic var date = ""
    
    func get (response : Dictionary<String, Any>) {
        self.status = response ["status"] as? Int ?? 0
        self.name = response ["name"] as? String ?? ""
        self.text = response ["text"] as? String ?? ""
        self.date = response ["date"] as? String ?? ""
    }
    
//                NSDictionary *mainDict = [[[json valueForKey:@"user"] valueForKey:@"user_data"] valueForKey:@"worksheets"];
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Form> {
        var array = Array<Form>()
        if let user = response["user"] as? [String:Any] {
            if let user_data = user["user_data"] as? [String:Any] {
                if let worksheets = user_data["worksheets"] as? [[String:Any]] {
                    for arrayDict in worksheets {
                        let object = Form()
                        object.get(response: arrayDict)
                        array.append(object)
                    }
                }
            }
        }
        return array
    }
}
