//
//  BonusessViewController.swift
//  PharmaCheck
//
//  Created by macOS on 10/28/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class BonusessViewController: ActivityViewController {
    
    @IBOutlet weak var tittleText : UILabel!
    @IBOutlet weak var bonusesTableView : UITableView!
    var bonusesURL = ""
    var howUseBonusesURL = ""
    
    //[Form]
    var formsArray = [Form]()
    
    @IBOutlet weak var howUseBonusses : UIButton!
    @IBOutlet weak var useBonusses : UIButton!
    
    @IBOutlet weak var showBonuses : NSLayoutConstraint!
    
    //@property (strong, nonatomic) InternetConnection *internetConnection;
    
    var historyListString = ""
    var historyBonusString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.useBonusses.roundCorners(value: 6)
        self.useBonusses.backgroundColor = .white
        self.useBonusses.addShadow()

        
        self.howUseBonusses.roundCorners(value: 6)
        self.howUseBonusses.backgroundColor = .white
        self.howUseBonusses.addShadow()

        self.bonusesTableView.isHidden = true
        self.start()
        RequestManager.shared.getUserWorkSheetsWithBonusessText(success: { (text, id, url, bonusURL, formArr) in
            
            self.tittleText.text = text
            
            self.bonusesURL = url
            self.howUseBonusesURL = bonusURL
            
            self.formsArray = formArr
            
            self.getHistoryListText()
            
        }) { errorCode in
            self.stop()
        }
    }
    
    func getHistoryListText () {
        RequestManager.shared.getHistoryList(success: { text in
            self.historyListString = text
            self.getHistoryBonusText()
        }) { errorCode in
            self.stop()
        }
    }
    
    func getHistoryBonusText () {
        RequestManager.shared.getHistoryBonus(success: { text in
            self.historyBonusString = text
            self.bonusesTableView.reloadData()
            self.bonusesTableView.isHidden = false
            self.stop()
        }) { errorCode in
            self.stop()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.bonusesTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! BonusesWebViewController
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        if segue.identifier == "howUseBonus" {
            vc.isUseBonus = true
            vc.bonusURL = self.howUseBonusesURL
        }
        if segue.identifier == "useBonus" {
            vc.bonusURL = self.bonusesURL
        }
    }
}

extension BonusessViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension BonusessViewController : UITableViewDataSource {
    
    func roundWithLayer (view : UIView) {
        view.layer.cornerRadius = view.bounds.size.width/2
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.gray.cgColor
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryBonusessCell", for: indexPath) as! HistoryBonusessCell
            let htmlString: String = self.historyBonusString
            let attrStr : NSAttributedString = try! NSAttributedString(data: htmlString.data(using: String.Encoding.unicode)!, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            cell.htmlText.attributedText = attrStr
            cell.backgroundShadowView.roundCorners(value: 6)
            cell.backgroundShadowView.addShadow()
            cell.selectionStyle = .none
            return cell
        }
        
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryFormsCell", for: indexPath) as! HistoryFormsCell
            let htmlString: String = self.historyListString
            let attrStr : NSAttributedString = try! NSAttributedString(data: htmlString.data(using: String.Encoding.unicode)!, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            cell.htmlText.attributedText = attrStr
            cell.backgroundShadowView.roundCorners(value: 6)
            cell.backgroundShadowView.addShadow()
            cell.selectionStyle = .none
            return cell
        }
        
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TiitleCell", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "BonuseCell", for: indexPath) as! BonuseCell
        let form = self.formsArray[indexPath.row - 3]
        
        cell.bonusDescription.text = form.text
        cell.bonusTextLabel.text = form.name
        cell.dateLabel.text = form.date
//        
        if form.status == 1 {
            cell.bonusActive.text = "Заявка подтверждена!"
            cell.activeView.backgroundColor = .green
        }
        if form.status == 0 {
            cell.bonusActive.text = "Заявка не подтверждена!"
            cell.activeView.backgroundColor = .yellow
        }
        if form.status == 2 {
            cell.bonusActive.text = "Заявка отклонена!"
            cell.activeView.backgroundColor = .red
            cell.bonusDescription.textColor = .red
        }

        cell.backgroundShadowView.addShadow()
        self.roundWithLayer(view: cell.activeView)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.formsArray.count + 3;
    }
}
