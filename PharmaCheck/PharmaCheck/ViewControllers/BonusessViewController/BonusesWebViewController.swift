//
//  BonusesWebViewController.swift
//  PharmaCheck
//
//  Created by macOS on 10/31/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class BonusesWebViewController: ActivityViewController, UIWebViewDelegate {
    
    var isUseBonus = false
    var bonusURL = ""
    
    @IBOutlet weak var webView : UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationTitle()
        self.startWithoutBlur()
        self.webView.delegate = self
        self.webView.loadRequest(URLRequest(url: URL(string: bonusURL)!))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.hideBurger()
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.navigationController is CustomNavigationController {
            let nc = self.navigationController as! CustomNavigationController
            nc.showBurger()
        }
    }
    
    func setNavigationTitle () {
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font:
            UIFont(name: FONT, size: 22)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController?.title = "БОНУСЫ".uppercased()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stopWithoutBlur()
        self.webView.isHidden = false
    }

}

