//
//  HistoryBonusessCell.swift
//  PharmaCheck
//
//  Created by macOS on 10/29/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class HistoryBonusessCell: UITableViewCell {
    
    @IBOutlet weak var htmlText : UILabel!
    @IBOutlet weak var backgroundShadowView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
