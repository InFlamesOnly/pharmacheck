//
//  BonuseCell.swift
//  PharmaCheck
//
//  Created by macOS on 10/29/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class BonuseCell: UITableViewCell {
    
    @IBOutlet weak var bonusDescription : UILabel!
    @IBOutlet weak var bonusActive : UILabel!
    @IBOutlet weak var bonusTextLabel : UILabel!
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var activeView : UIView!
    @IBOutlet weak var backgroundShadowView : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundShadowView.layer.cornerRadius = 6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
