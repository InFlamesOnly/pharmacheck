//
//  CalcSKFVC.swift
//  StarClub
//
//  Created by Dima on 12.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class CalcSKFVC: UIViewController {
    
    @IBOutlet weak var rezultButton: CornerRoundsButton!
    
    @IBOutlet weak var genderDropDown : DropDown!
    @IBOutlet weak var ageTextField : UITextField!
    @IBOutlet weak var weightTextField : UITextField!
    @IBOutlet weak var creatinTextField : UITextField!
    @IBOutlet weak var mochTextField : UITextField!
    @IBOutlet weak var albTextField : UITextField!
    
//    @IBOutlet weak var rezultLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        self.genderDropDown.optionArray = ["Мужской", "Женский"]
        
        self.genderDropDown.didSelect{(selectedText , index ,id) in
            self.genderDropDown.placeholder = selectedText
            self.genderDropDown.textAlignment = .center
        }
        self.viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        self.rezultButton.defaultInitializer()
    }
    
    @IBAction func calculate(_ sender: Any) {
        var gender = 0
        if  genderDropDown.placeholder == "Женский" || genderDropDown.text == "Женский" {
            gender = 1
        }
        
        if self.ageTextField.text?.count == 0 && self.weightTextField.text?.count == 0 && self.creatinTextField.text?.count == 0 && self.mochTextField.text?.count == 0 && self.albTextField.text?.count == 0 {
            self.alert(message: "Заполните все поля")
            return
        }
        
        if self.ageTextField.text?.count == 0 {
            self.alert(message: "Укажите ваш возраст")
            return
        }
        
        if self.weightTextField.text?.count == 0 {
            self.alert(message: "Укажите ваш вес")
            return
        }
        
        if self.creatinTextField.text?.count == 0 {
            self.alert(message: "Укажите креатинин")
            return
        }
        
        if self.mochTextField.text?.count == 0 {
            self.alert(message: "Укажите мочевину")
            return
        }
        
        if self.albTextField.text?.count == 0 {
            self.alert(message: "Укажите альбумин")
            return
        }

        self.alert(message: self.calculate(gender: gender, age: Int(self.ageTextField.text!)!, weight: Double(self.weightTextField.text!)!, creatin: Double(self.creatinTextField.text!)!, moch: Double(self.mochTextField.text!)!, alb: Double(self.albTextField.text!)!), title: "Результат")
    }

    func calculate (gender : Int, age : Int, weight : Double, creatin : Double, moch : Double, alb : Double) -> String {
        var returnedString = ""
        if gender == 0 {//m
            let res = (Double((140 - age)) * weight * 1.2) / creatin
            let roundRezClCr = "\(Int(res) )" + " " + "мл/мин" //res Клиренс креатин
            returnedString = "КК (формула Кокрофта - Голта) : " + roundRezClCr + "\n"
            let a = creatin * 0.0113
            let b = moch * 6
            let c = alb * 0.1

            let rezMDR = 170 * pow(a, -0.999) * pow(Double(age), -0.176) * pow(b, -0.170) * pow(c, 0.318)
            let roundRezMDR = "\(Int(rezMDR) )" + " " + "мл/мин/1.73 м²"
            returnedString = returnedString + "Скорость клубочковой фильтрации (MDRD) : " + roundRezMDR + "\n"
            
            if creatin <= 80 {
                let d = creatin * 0.0113/0.9
                let resCKD = Double(141) * pow(0.993, Double(age)) * pow(d, -0.412)
                let roundRezCKD = "\(Int(resCKD) )" + " " + "мл/мин/1.73 м²"
                returnedString = returnedString + "Скорость клубочковой фильтрации (CKD-EPI) :" + roundRezCKD
            } else {
                let d = creatin * 0.0113/0.9
                let resCKD = Double(141) * pow(0.993, Double(age)) * pow(d, -1.21)
                let roundRezCKD = "\(Int(resCKD) )" + " " + "мл/мин/1.73 м²"
                returnedString = returnedString + "Скорость клубочковой фильтрации (CKD-EPI) : " + roundRezCKD
            }
        } else {
            let res = (Double((140 - age)) * weight * 1.02) / creatin
            let roundRezClCr = "\(Int(res) )" + " " + "мл/мин" //res Клиренс креатин
            returnedString = "КК (формула Кокрофта - Голта) : " + roundRezClCr + "\n"
            let a = creatin * 0.0113
            let b = moch * 6
            let c = alb * 0.1
            
            let rezMDR = 170 * pow(a, -0.999) * pow(Double(age), -0.176) * pow(b, -0.170) * pow(c, 0.318) * 0.762
            let roundRezMDR = "\(Int(rezMDR) )" + " " + "мл/мин/1.73 м²"
            returnedString = returnedString + "Скорость клубочковой фильтрации (MDRD) : " + roundRezMDR + "\n"
            if creatin <= 62 {
                let d = creatin * 0.0113/0.7
                let resCKD = Double(144) * pow(0.993, Double(age)) * pow(d, -0.328)
                let roundRezCKD = "\(Int(resCKD) )" + " " + "мл/мин/1.73 м²"
                returnedString = returnedString + "Скорость клубочковой фильтрации (CKD-EPI) : " + roundRezCKD
            } else {
                let d = creatin * 0.0113/0.7
                let resCKD = Double(144) * pow(0.993, Double(age)) * pow(d, -1.21)
                let roundRezCKD = "\(Int(resCKD) )" + " " + "мл/мин/1.73 м²"
                returnedString = returnedString + "Скорость клубочковой фильтрации (CKD-EPI) : " + roundRezCKD
            }
        }
        return returnedString
    }
}
