//
//  CustomNavigationController.swift
//  Remedy
//
//  Created by macOS on 22.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

class CustomNavigationController: UINavigationController {
    
    var titleText = ""
    private let imageView = UIImageView(image: UIImage(named: "NavLogo"))
    private let burgerButton = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageToNavigation()
        self.crateBurgerButton()
    }
    
    override func viewDidLayoutSubviews() {
        self.setGradient()
    }
        
    private func createNavigationItems () {
        let navigationItem = UINavigationItem()
        let burgerIcon = self.createBurgerIcon()
        
        navigationItem.leftBarButtonItem = burgerIcon
        
        self.navigationBar.items = [navigationItem]
        self.view.addSubview(navigationBar)
        self.imageToNavigation()
    }
    
    func hideBurger () {
        self.burgerButton.isHidden = true
    }
    
    func showBurger () {
        self.burgerButton.isHidden = false
    }
    
    func imageToNavigation () {
        self.navigationBar.addSubview(imageView)
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: 0),
            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: 2)
            ,
            imageView.heightAnchor.constraint(equalToConstant: 52),
            imageView.widthAnchor.constraint(equalToConstant: 100),
        ])
        imageView.contentMode = .scaleAspectFit
    }
    
    func crateBurgerButton () {
        let icon = UIImage(named: "Burger")
        burgerButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        burgerButton.imageView?.contentMode = .scaleAspectFill
        burgerButton.setImage(icon, for: .normal)
        self.navigationBar.addSubview(burgerButton)
        burgerButton.clipsToBounds = true
        burgerButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            burgerButton.leftAnchor.constraint(equalTo: navigationBar.leftAnchor, constant: 0),
            burgerButton.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: 2)
            ,
            burgerButton.heightAnchor.constraint(equalToConstant: 52),
            burgerButton.widthAnchor.constraint(equalToConstant: 72),
        ])
        burgerButton.contentMode = .scaleAspectFit
    }
    
    private func configureTittle () {
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.font:
            UIFont(name: FONT, size: 22)!,
                                                  NSAttributedString.Key.foregroundColor:UIColor.white]
        self.checkFirstCreateNavigation()
        self.navigationBar.topItem!.title = titleText
    }
    
    private func checkFirstCreateNavigation () {
//        if titleText == "" {
//            titleText = "ИССЛЕДОВАНИЯ"
////            titleText = "ЛЕНТА"
//        }
    }
    
    private func setGradient () {
        var colors = [UIColor]()
        colors.append(BLUE_COLOR)
        colors.append(BLUE_COLOR)
        self.navigationBar.setGradientBackground(colors: colors)
    }
    
    private func createBurgerIcon () -> UIBarButtonItem {
        let icon = UIImage(named: "Burger")
        let iconButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 40, height: 40)))
        iconButton.setImage(icon, for: .normal)
        let barButton = UIBarButtonItem(customView: iconButton)
        iconButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        iconButton.imageView?.contentMode = .scaleAspectFill
        return barButton
    }
    
    @objc func showMenu() {
        print("show menu")
        let drawerController = self.parent as? KYDrawerController
        drawerController?.setDrawerState(KYDrawerController.DrawerState.opened, animated: true)
    }

}

extension CAGradientLayer {
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
        startPoint = CGPoint(x: 0, y: 0)
        endPoint = CGPoint(x: 0, y: 1)
    }
    
    func createGradientImage() -> UIImage? {
        
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
    
}

extension UINavigationBar {
    func setGradientBackground(colors: [UIColor]) {
        
        var updatedFrame = bounds
        updatedFrame.size.height += self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        
        setBackgroundImage(gradientLayer.createGradientImage(), for: UIBarMetrics.default)
    }
}


extension UIViewController {
            open override func awakeFromNib() {
                navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
}
