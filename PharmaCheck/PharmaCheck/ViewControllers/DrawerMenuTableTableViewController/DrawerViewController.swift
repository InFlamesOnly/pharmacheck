//
//  DrawerViewController.swift
//  Remedy
//
//  Created by macOS on 22.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

private let START_NAVIGATION_CONTROLLER = "StartNavigationViewController"

class DrawerViewController: UIViewController {
    
    @IBAction func exit(_ sender: Any) {
        let vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: START_NAVIGATION_CONTROLLER) as? UINavigationController)!
        if #available(iOS 13.0, *) {
            vc.modalPresentationStyle = .fullScreen
        }
        self.present(vc, animated: true, completion: nil)
    }
    

}
