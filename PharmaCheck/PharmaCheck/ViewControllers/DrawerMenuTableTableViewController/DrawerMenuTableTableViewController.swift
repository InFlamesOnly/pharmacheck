//
//  DrawerMenuTableTableViewController.swift
//  Remedy
//
//  Created by macOS on 22.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

private let CELL_ID = "MenuCell"


private let MAIN = "Main"
private let PROGRESS = "Progress"
private let PROFILE = "Profile"
private let RESEARCHES = "Researches"
private let SUPPORT = "Support"
private let BONUS = "Bonusess"
private let IMT = "CalculatorVC"
private let SCORE = "CalculatorVCSCORE"
private let SCF = "CalculatorSKF"
private let СHAT = "ChatVC"

private let CALENDAR = "CalendarVC"

class DrawerMenuTableViewController: UITableViewController {
    //Чат
    var isOpen = false
//    let arraysControllers = [MENU, PROGRESS, PROFILE, RESEARCHES, SUPPORT, СHAT, CALENDAR, IMT, SCORE, SCF]
    let arraysControllers = [MAIN, RESEARCHES, CALENDAR, SUPPORT, BONUS, PROFILE]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(MenuCell.self, forCellReuseIdentifier: CELL_ID)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let drawerController = navigationController?.parent as? KYDrawerController
        
        var vc = CustomNavigationController()
        
        switch indexPath.row {
        case 0: break
        case 1:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: MAIN) as? CustomNavigationController)!
//            vc.titleText = "Лента"
            break
        case 2:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: RESEARCHES) as? CustomNavigationController)!
//            vc.titleText = "Исследования"
            break
        case 3:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: CALENDAR) as? CustomNavigationController)!
//            vc.titleText = "Календарь"
            break
        case 4:
            vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: SUPPORT) as? CustomNavigationController)!
            //            vc.titleText = "Календарь"
            break
            case 5:
                vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: BONUS) as? CustomNavigationController)!
                //            vc.titleText = "Календарь"
                break
            
            case 6:
                vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: PROFILE) as? CustomNavigationController)!
                //            vc.titleText = "Календарь"
                break
        default:
            break
        }
        
        if indexPath.row != 0 {
            drawerController?.mainViewController = vc
            drawerController?.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
            self.tableView.reloadData()
        }
    }
    
    @IBAction func close (_ sender : UIButton) {
        let drawerController = navigationController?.parent as? KYDrawerController
        drawerController?.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
    }
    
    func update () {
        self.isOpen = !self.isOpen
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at:[
//                                    IndexPath(item: 6, section: 0),
//                                    IndexPath(item: 7, section: 0),
//                                    IndexPath(item: 8, section: 0)
            ], with: UITableView.RowAnimation.none)
        self.tableView.endUpdates()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if !isOpen {
//            if indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10 {
//                return 0
//            }
//        } else {
//           return 59
//        }
        return 59
    }
}
