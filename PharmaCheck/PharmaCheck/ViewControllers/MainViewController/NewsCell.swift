//
//  NewsCell.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    @IBOutlet weak var newsImageView : UIImageView!
    @IBOutlet weak var newsTittle : UILabel!
    @IBOutlet weak var newsSubTittle : UILabel!
    @IBOutlet weak var newsCategory : UILabel!
    @IBOutlet weak var newsDate : UILabel!
    @IBOutlet weak var bckView : UIView!
    @IBOutlet weak var categoryImageView : UIImageView!
    @IBOutlet weak var isFavoriteNews : UIButton!
    
    func configure () {
        self.newsImageView.roundCorners(value: 6)
        self.bckView.roundCorners(value: 6)
        self.selectionStyle = .none
    }
}
