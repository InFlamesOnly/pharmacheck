//
//  MainViewController.swift
//  Remedy
//
//  Created by macOS on 24.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

private let TEST_CELL_ID = "TestsCell"
private let TEST_INFO_CELL_ID = "TestInfoCell"
private let SEPARATOR_CELL_ID = "SeparatorCell"
private let NEWS_CELL_ID = "NewsCell"

class MainViewController: ActivityViewController {
    
    @IBOutlet weak var mainTableView : UITableView!
    
    var articles : Array<Article> = Array()
    var selectedArticles : Article?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getContent()
    }
    
    func getContent () {
        self.startWithoutBlur()
        RequestManager.shared.content(success: { (responseObject) in
            self.articles = Article.mapResponseToArrayObject(response: responseObject)
            self.stopWithoutBlur()
            self.mainTableView.reloadData()
            self.mainTableView.isHidden = false
        }) { (errorCode) in
            self.stopWithoutBlur()
            self.mainTableView.isHidden = false
            self.getContent()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailArticle" {
            let vc = segue.destination as! DetailMainViewController
            vc.article = self.selectedArticles
        }
    }
}

extension MainViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = self.articles[indexPath.row]
        self.selectedArticles = article
        self.performSegue(withIdentifier: "showDetailArticle", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension MainViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let article = self.articles[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: NEWS_CELL_ID) as! NewsCell
        cell.newsTittle.text = article.tittle
        cell.newsSubTittle.text = article.shortContent
        
        cell.newsImageView.roundCorners(value: 6)
        cell.bckView.roundCorners(value: 6)
        
        Alamofire.request(article.imagePath).responseImage { response in
            if let image = response.result.value {
                cell.newsImageView.image = image
            }
        }
        cell.newsCategory.text = article.category
        cell.newsDate.text = article.author
        self.switchCellCategory(name: article.category, cell: cell)
//        cell.newsCompany.text = article.author
        cell.selectionStyle = .none
//        let cell = tableView.dequeueReusableCell(withIdentifier: TEST_CELL_ID, for: indexPath) as! TestsCell
//        if indexPath.row == 1 {
//            return tableView.dequeueReusableCell(withIdentifier: SEPARATOR_CELL_ID) as! SeparatorCell
//        }
//        if indexPath.row > 1 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: NEWS_CELL_ID, for: indexPath) as! NewsCell
//            cell.configure()
//           return cell
//        }
//        cell.setupFlowLayaut()
        return cell
    }
    
    func switchCellCategory (name : String, cell : NewsCell) {
        if name == "Статьи" {
            cell.newsCategory.textColor = BLUE_COLOR
            cell.categoryImageView.image = UIImage(named: "CellGrey")
        } else if name == "Тесты" {
            cell.newsCategory.textColor = UIColor.white
            cell.categoryImageView.image = UIImage(named: "CellBlue")
        } else {
            cell.newsCategory.textColor = UIColor.white
            cell.categoryImageView.image = UIImage(named: "CellRed")
        }
    }
}

extension MainViewController : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 67, height: 220)
    }
}

extension MainViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TEST_INFO_CELL_ID, for: indexPath) as! TestInfoCell
        cell.configure()
        return cell
    }
}
