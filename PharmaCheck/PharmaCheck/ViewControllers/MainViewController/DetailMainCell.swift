//
//  DetailMainCell.swift
//  Remedy
//
//  Created by Dima on 17.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RichTextView

class DetailMainCell: UITableViewCell {
    
//    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var articleTittle : UILabel!
    @IBOutlet weak var articleHTMLText : UILabel!
    @IBOutlet weak var htmlTextView : RichTextView!
    

}
