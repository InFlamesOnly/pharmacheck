//
//  SeparatorCell.swift
//  Remedy
//
//  Created by macOS on 24.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class SeparatorCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
}
