//
//  ViewController.swift
//  Remedy
//
//  Created by macOS on 22.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

private let LOGIN_ID = "user_is_login"
private let NOT_LOGIN_ID = "user_is_not_login"

class SplashScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.startTimer()
        self.configureDrawer()
    }
        
    private func startTimer () {
        _ = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            self.checkFirstScreen()
        }
    }
    
    private func checkFirstScreen () {
        if User.userIsFind() == false {
            self.performSegue(withIdentifier: NOT_LOGIN_ID, sender: self)
        } else {
            if User.currentUser().isAuth == 1 {
                self.performSegue(withIdentifier: LOGIN_ID, sender: self)
            } else {
                self.performSegue(withIdentifier: NOT_LOGIN_ID, sender: self)
            }
        }
    }
    
    private func configureDrawer () {
        let vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: "KYDrawerController") as? KYDrawerController)!
        vc.drawerWidth = UIScreen.main.bounds.width * 0.9
    }
}

