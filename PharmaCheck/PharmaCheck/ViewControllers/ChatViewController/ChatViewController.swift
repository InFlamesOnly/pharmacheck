//
//  ChatViewController.swift
//  Remedy
//
//  Created by Dima on 18.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ChatViewController: ActivityViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView : UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationTitle()
        self.startWithoutBlur()
        self.webView.delegate = self
        self.webView.loadRequest(URLRequest(url: URL(string: "https://chat.remedy-ua.com/1.php?token=\(User.currentUser().token)")!))
    }
    
    func setNavigationTitle () {
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font:
            UIFont(name: FONT, size: 22)!, NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController?.title = "ЧАТ".uppercased()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stopWithoutBlur()
        self.webView.isHidden = false
    }
}
