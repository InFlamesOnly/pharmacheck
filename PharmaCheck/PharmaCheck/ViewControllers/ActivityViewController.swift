//
//  ActivityViewController.swift
//  Remedy
//
//  Created by macOS on 05.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import SpringIndicator

class LightViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            overrideUserInterfaceStyle = .light
        }
    }
}

class ActivityViewController: LightViewController {
    
    var loaderView : LoaderView?
    var indicator : SpringIndicator?

    func start () {
        self.loaderView = self.createLoaderView()
        self.view.addSubview(self.loaderView!)
    }
    
    func stop () {
        self.loaderView!.removeFromSuperview()
    }
    
    private func createLoaderView () -> LoaderView {
        let loaderView = LoaderView()
        loaderView.startFromView(view: self.view)
        return loaderView
    }
    
    func startWithoutBlur () {
        self.addIndicator()
    }
    
    func stopWithoutBlur () {
        self.indicator?.removeFromSuperview()
    }
    
    private func addIndicator () {
        self.indicator = SpringIndicator()
        self.indicator!.frame.size = CGSize(width: 100, height: 100)
        self.indicator!.center = self.view.center
        self.indicator!.lineColor = FIRST_GRADIENT_COLOR
        self.view.addSubview(indicator!)
        self.indicator!.start()
    }
}
