//
//  ChangeAccView.swift
//  Remedy
//
//  Created by macOS on 29.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

protocol ChangeAccViewDelegate : class {
    func change (account : Account)
}

private let ACC_CELL_ID = "ChangeAccCell"

class ChangeAccView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var accountsTableView: UITableView!
    @IBOutlet weak var heightTableViewConstraint: NSLayoutConstraint!
    
    weak var delegate : ChangeAccViewDelegate?
    
    var accounts : Array<Account> = Array()
    
    func loadView (frame : CGRect) ->  ChangeAccView {
        let view = Bundle.main.loadNibNamed("ChangeAccView", owner: self, options: nil)?.first as! ChangeAccView
        view.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        view.accountsTableView.register(UINib(nibName: ACC_CELL_ID, bundle: nil), forCellReuseIdentifier: ACC_CELL_ID)
        self.addSubview(view)
        view.contentView.roundCorners(value: 10)
        view.heightTableViewConstraint.constant = 71 * 2
        return view
    }
    
    func update (accounts : Array <Account>) {
        self.heightTableViewConstraint.constant = CGFloat(71 * accounts.count)
        self.accounts = accounts
        self.accountsTableView.reloadData()
    }
    
    @IBAction func close(_ sender: Any) {
        self.removeFromSuperview()
        print("Close")
    }
    
    @IBAction func save(_ sender: Any) {
        self.removeFromSuperview()
        print("Close")
    }
}

extension ChangeAccView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 71
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.change(account: self.accounts[indexPath.row])
        self.close(self)
    }
}

extension ChangeAccView : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.accounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ACC_CELL_ID, for: indexPath) as! ChangeAccCell
        cell.configure(account: self.accounts[indexPath.row])
        return cell
    }
}
