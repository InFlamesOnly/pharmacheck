//
//  ProfileViewController.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

private let START_NAVIGATION_CONTROLLER = "StartNavigationViewController"
private let CELL_ID = "ProfileCell"

class ProfileViewController: ActivityViewController {
    
    @IBOutlet weak var profileTableView : UITableView!
    var placess : Array<Place> = Array()
    var accounts : Array<Account> = Array()
    
    let user = User.currentUser()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTableView.isHidden = true
        self.getAccounts()
        self.getProfile()
    }
    
    func getProfile () {
        self.startWithoutBlur()
        RequestManager.shared.myProfile(success: { (responseObject) in
            self.placess = Place.mapResponseToArrayObject(response: responseObject)
            self.user.updateBonuses(response: responseObject)
            self.stopWithoutBlur()
            self.profileTableView.isHidden = false
            self.profileTableView.reloadData()
        }) { (errorCode) in
            self.stopWithoutBlur()
        }
    }

    func getAccounts () {
        RequestManager.shared.acoounts(success: { (responseObject) in
            self.accounts = Account.mapResponseToArrayObject(response: responseObject)
        }) { (errorCode) in
            
        }
    }
}

extension ProfileViewController : CustomSwitchDelegate {
    func on() {
        self.user.notificationOn()
        //Request to on
    }
    
    func off() {
        self.user.notificationOff()
        //Request to off
    }
}

extension ProfileViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 268
        }
        if indexPath.row == 2 {
            return 98
        }
        if indexPath.row == 4 {
            return 0
        }
        return 54
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 4 {
            self.createChangePasswordView()
        }
        if indexPath.row == 5 {
            let user = User.currentUser()
            user.isAuth(auth: false)
            self.exit()
        }
    }
    
    func createChangePasswordView () {
        let view = ChangePasswordView().loadView(frame: self.view.frame)
        self.view.addSubview(view)
        view.delegate = self
    }
    
    func exit () {
        let vc = (UIStoryboard(name: STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: START_NAVIGATION_CONTROLLER) as? UINavigationController)!
        self.present(vc, animated: true, completion: nil)
    }
}

extension ProfileViewController : ChangePasswordViewDelegate {
    func save(oldPassword : String, newPassword : String) {
        //TODO new password
    }
}

extension ProfileViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(CELL_ID)\(indexPath.row)", for: indexPath) as! ProfielCell
        if indexPath.row == 0 {
            cell.avatarView!.addSubview(AvatarView().avatarWithGradient (bounds : cell.avatarView!.bounds))
            cell.changeAcc!.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        }
        cell.notificationSwitch?.delegate = self
        cell.configure()
        cell.configure(from: self.placess)
        cell.selectionStyle = .none
        return cell
    }
    
    @objc func buttonAction(sender: UIButton!) {
        let view = ChangeAccView().loadView(frame: self.view.frame)
        self.view.addSubview(view)
        view.update(accounts: self.accounts)
        view.delegate = self
    }
}

extension ProfileViewController : ChangeAccViewDelegate {
    func change(account: Account) {
        self.getUser(token: account.token)
    }
    
    func getUser(token : String) {
        self.startWithoutBlur()
        self.profileTableView.isHidden = true
        RequestManager.shared.userFromToken(token: token, success: { (responseObject) in
            let firebaseToken = User.currentUser().firebaseToken
            let deviceToken = User.currentUser().deviceToken
            User.remove()
            self.saveUser(responseObject: responseObject)
            self.user.updateDeviceToken(deviceToken: deviceToken)
            self.user.updateFCMToken(firebaseToken: firebaseToken)
            self.stopWithoutBlur()
            self.getAccounts()
            self.getProfile()
        }) { (errorCode) in
            self.profileTableView.isHidden = false
            self.stopWithoutBlur()
        }
    }
    
    private func saveUser (responseObject: Dictionary<String, Any>) {
        let user = User()
        user.get(response: responseObject)
        user.save()
        user.isAuth(auth: true)
    }

}
