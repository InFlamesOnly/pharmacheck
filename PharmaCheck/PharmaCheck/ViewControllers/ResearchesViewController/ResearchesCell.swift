//
//  ResearchesCell.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

protocol CellDelegate: class {
    func didTapGoToReserch(_ cell: ResearchesCell)
    func didTapGoToPop(_ cell: ResearchesCell)
}

class ResearchesCell: UITableViewCell {
    
    @IBOutlet weak var bckView : UIView!
    @IBOutlet weak var researchesActiveDate : UILabel!
    @IBOutlet weak var researchesName : UILabel!
//    @IBOutlet weak var researchesPop : UILabel!
    @IBOutlet weak var goToReserch : UIButton!
    @IBOutlet weak var goToPop : UIButton!
    @IBOutlet weak var researchesStatus : UIView!
    
    weak var delegate: CellDelegate?
    
    func configure (research : Research) {
        self.researchesStatus.roundView()
        self.bckView.roundCorners(value: 6)
        self.bckView.addShadow()
        self.cellFromResearch(research: research)
        self.selectionStyle = .none
    }
    
    func cellFromResearch (research : Research) {
        self.researchesActiveDate.text = research.activeTill
        self.researchesName.text = research.name
//        self.configPaP(research: research)
    }
    
//    func configPaP (research : Research) {
//        if research.isNeedShowPaP {
//            self.researchesPop.text = "Условия исследования"
//        } else {
//            self.researchesPop.text = "Перейти в  исследование"
//        }
//    }
    
    @IBAction func didTapGoToReserchPressed(_ sender: Any) {
        delegate?.didTapGoToReserch(self)
    }
    
    @IBAction func didTapGoToPopPressed(_ sender: Any) {
        delegate?.didTapGoToPop(self)
    }

}
