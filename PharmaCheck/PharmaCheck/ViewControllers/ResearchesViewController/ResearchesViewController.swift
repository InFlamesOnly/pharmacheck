//
//  ResearchesViewController.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import CoreLocation

private let RESEARCHES_CELL_ID = "ResearchesCell"
private let FORMS_ID = "go_to_forms"


class ResearchesViewController: ActivityViewController , CellDelegate{
    
    @IBOutlet weak var researchedTableView : UITableView!
    
    var researches : Array<Research> = Array()
    var placess : Array<Place> = Array()
    
    var myLocation = CLLocation()
    var locationManager = CLLocationManager()
    
    var url = ""
    var formurl = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getProfile()
        self.getUserLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getResearch()
    }
    
    func getUserLocation () {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        if let location = self.locationManager.location {
            self.myLocation = location
        } else {
            self.presentAlert(withTitle: "Ошибка", message: "Геопозиция не включена")
        }
    }
    
    func getProfile () {
        RequestManager.shared.myProfile(success: { (responseObject) in
            self.placess = Place.mapResponseToArrayObject(response: responseObject)
        }) { (errorCode) in
            
        }
    }
    
    @IBAction func buttonTapped ( sender : UIButton) {
    
    }
    
    func userIsCorrectPlaseCordinate () -> Bool {
        var isOk = false
        for place in self.placess {
            let location = CLLocation(latitude: place.latitude, longitude: place.longitude)
            let distance = self.myLocation.distance(from: location)
            
            if distance < 1000 {
                isOk = true
                break;
            }
            
        }
        return isOk
    }
    
    func getResearch () {
        self.researchedTableView.isHidden = true
        self.startWithoutBlur()
        RequestManager.shared.researchList(success: { (responseObject) in
            self.researches = Research.mapResponseToArrayObject(response: responseObject)
            self.stopWithoutBlur()
            self.researchedTableView.reloadData()
            self.researchedTableView.isHidden = false
        }) { (errorCode) in
            self.getResearch()
            self.stopWithoutBlur()
            self.researchedTableView.isHidden = false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == FORMS_ID {
            let vc = segue.destination as! FormsViewController
            vc.url = self.url
        }
    }
}

extension ResearchesViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let research = self.researches[indexPath.row]
        self.showFormsViewController(research: research, isPop: false)
    }
    
    func showFormsViewController (research : Research, isPop : Bool) {
        if !research.isNeedShowPaP {
            self.url = research.policyURL
        } else if research.isNeedShowPaP && isPop  {
            self.url = research.policyURL
        } else {
            self.url = research.formURL
        }
        self.performSegue(withIdentifier: FORMS_ID, sender: self)
    }
}

extension ResearchesViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return researches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RESEARCHES_CELL_ID, for: indexPath) as! ResearchesCell
        let research = self.researches[indexPath.row]
        cell.configure(research: research)
        cell.delegate = self
        return cell
    }
    
    func didTapGoToReserch(_ cell: ResearchesCell) {
        let indexPath = self.researchedTableView.indexPath(for: cell)
        let research = self.researches[indexPath!.row]
        self.showFormsViewController(research: research, isPop: false)

    }
    
    func didTapGoToPop(_ cell: ResearchesCell) {
        let indexPath = self.researchedTableView.indexPath(for: cell)
        let research = self.researches[indexPath!.row]
        self.showFormsViewController(research: research, isPop: true)
    }
}

extension ResearchesViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.myLocation = manager.location!
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
}
