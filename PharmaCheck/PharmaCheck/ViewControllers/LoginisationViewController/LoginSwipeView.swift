//
//  LoginSwipeView.swift
//  Remedy
//
//  Created by macOS on 23.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol LoginSwipeViewDelegate : class {
    func login(phone : String, password : String)
    func get (phone : String)
    func forgotPassword ()
}

private enum Errors : String {
    case Phone = "Введите номер телефона!"
    case Password = "Введите пароль!"
    case PhoneValid = "Введите валидный номер телефона!"
}

let TELEPHONECODE = "+38 "

class LoginSwipeView: UIView {
    
    weak var delegate : LoginSwipeViewDelegate?
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var loginTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var loginImageView: UIImageView!
    @IBOutlet weak var passwordImageView: UIImageView!
    
    var phone = ""
    
    func loadView (frame : CGRect) ->  LoginSwipeView {
        let view = Bundle.main.loadNibNamed("LoginSwipeView", owner: self, options: nil)?.first as! LoginSwipeView
        view.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        view.contentView.hideKeyboardWhenTappedAround()
        self.addSubview(view)
        return view
    }
    
    @IBAction func login(_ sender: Any) {
        if self.checkValidationFields() {
            self.delegate?.login(phone : self.loginTextField.text!.filterPhone(), password : self.passwordTextField.text!)
        }
    }
    
    @IBAction func forgotPassword (_ sender: Any) {
        self.delegate?.forgotPassword()
    }
    
    func getPhone () {
        self.delegate?.get(phone: self.loginTextField.text!.filterPhone())
    }
    
    private func checkValidationFields () -> Bool {
        if self.loginTextField.text?.count == 0 {
            self.showError(message: Errors.Phone.rawValue)
            return false
        }
        if self.passwordTextField.text?.count == 0 {
            self.showError(message: Errors.Password.rawValue)
            return false
        }
        if (self.loginTextField.text?.count)! < 19 {
            self.showError(message: Errors.PhoneValid.rawValue)
            return false
        }
        return true
    }
    
    private func showError (message : Errors.RawValue) {
        if let topController = UIApplication.topViewController() {
            topController.presentAlert(withTitle: "Ошибка", message: message)
        }
    }
}

extension LoginSwipeView : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == loginTextField {
            if textField.text?.count == 0 {
                textField.text = TELEPHONECODE
            }
            self.loginImageView.image = UIImage(named: "LoginActive")
        } else if textField == passwordTextField {
            self.passwordImageView.image = UIImage(named: "PasswordActive")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == loginTextField {
            if textField.text == TELEPHONECODE {
                textField.text = ""
            }
            self.loginImageView.image = UIImage(named: "Login")
        } else if textField == passwordTextField {
            self.passwordImageView.image = UIImage(named: "Password")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == loginTextField {
            if range.location < 19 {
                return TelephoneNumberValidator.formattedTextField(textField, shouldChangeCharactersIn: range, replacementString: string)
            } else {
                return false
            }
        }
        return true
    }
}
