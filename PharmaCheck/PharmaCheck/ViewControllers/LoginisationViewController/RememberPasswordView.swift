//
//  RememberPasswordView.swift
//  Remedy
//
//  Created by macOS on 23.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol RememberPasswordViewDelegate : class {
    func getPassword (phone : String)
    func back()
}

private enum Errors : String {
    case Phone = "Введите номер телефона!"
    case PhoneValid = "Введите валидный номер телефона!"
}

class RememberPasswordView: UIView {

    weak var delegate : RememberPasswordViewDelegate?
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var loginTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneImageView: UIImageView!
    
    func loadView (frame : CGRect) ->  RememberPasswordView {
        let view = Bundle.main.loadNibNamed("RememberPasswordView", owner: self, options: nil)?.first as! RememberPasswordView
        view.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        view.contentView.hideKeyboardWhenTappedAround()
        self.addSubview(view)
        return view
    }
    
    @IBAction func back(_ sender: Any) {
        self.delegate?.back()
        print("back")
    }
    
    @IBAction func getPassword(_ sender: Any) {
//        if self.checkValidationFields() {
            self.delegate?.getPassword(phone: loginTextField.text!)
//        }
    }
    
    private func checkValidationFields () -> Bool {
        if self.loginTextField.text?.count == 0 {
            self.showError(message: Errors.Phone.rawValue)
            return false
        }
        if (self.loginTextField.text?.count)! < 19 {
            self.showError(message: Errors.PhoneValid.rawValue)
            return false
        }
        return true
    }
    
    private func showError (message : Errors.RawValue) {
        if let topController = UIApplication.topViewController() {
            topController.presentAlert(withTitle: "Ошибка", message: message)
        }
    }
}

extension RememberPasswordView : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == loginTextField {
            if textField.text?.count == 0 {
                textField.text = TELEPHONECODE
            }
            self.phoneImageView.image = UIImage(named: "LoginActive")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == loginTextField {
            if textField.text == TELEPHONECODE {
                textField.text = ""
            }
            self.phoneImageView.image = UIImage(named: "Login")
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == loginTextField {
            if range.location < 19 {
                return TelephoneNumberValidator.formattedTextField(textField, shouldChangeCharactersIn: range, replacementString: string)
            } else {
                return false
            }
        }
        return true
    }
}
