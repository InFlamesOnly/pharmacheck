//
//  LoginisationViewController.swift
//  Remedy
//
//  Created by macOS on 23.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import FirebaseMessaging

private let LOGIN_ID = "user_is_login"

class LoginisationViewController: ActivityViewController {
    
    @IBOutlet weak var viewPager: ViewPager!
    
    var loginSwipeView : LoginSwipeView?
    var rememberPasswordSwipeView : RememberPasswordView?
    var newPasswordSwipeView : NewPasswordView?
    
    var phone = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewPager.dataSource = self
        self.hideKeyboardWhenTappedAround()
    }
}

extension LoginisationViewController : ViewPagerDataSource {
    func numberOfItems(viewPager: ViewPager) -> Int {
        return 3
    }
    
    func viewAtIndex(viewPager: ViewPager, index: Int, view: UIView?) -> UIView {
        switch index {
        case 0:
            guard (self.loginSwipeView != nil) else {
                self.loginSwipeView = LoginSwipeView().loadView(frame: self.viewPager.frame)
                self.loginSwipeView!.delegate = self
                return self.loginSwipeView!
            }
            return self.loginSwipeView!
        case 1:
            guard (self.rememberPasswordSwipeView != nil) else {
                self.rememberPasswordSwipeView = RememberPasswordView().loadView(frame: self.viewPager.frame)
                self.rememberPasswordSwipeView!.delegate = self
                return self.rememberPasswordSwipeView!
            }
            return self.rememberPasswordSwipeView!
        case 2:
            guard (self.newPasswordSwipeView != nil) else {
                self.newPasswordSwipeView = NewPasswordView().loadView(frame: self.viewPager.frame)
                self.newPasswordSwipeView!.delegate = self
                return self.newPasswordSwipeView!
            }
            return self.newPasswordSwipeView!
        default:
            return UIView()
        }
    }
}

extension LoginisationViewController : LoginSwipeViewDelegate {
    func forgotPassword() {
        self.viewPager.scrollToPage(index: 2)
        self.loginSwipeView!.getPhone()
    }
    
    func login(phone : String, password : String) {
        self.auth(phone: phone, password: password)
    }
    
    func get (phone : String) {
        self.rememberPasswordSwipeView?.loginTextField.text = TelephoneNumberValidator.formattedNumber(number: phone)
        print("\(phone)")
    }
}

extension LoginisationViewController : RememberPasswordViewDelegate {
    func getPassword(phone: String) {
        self.getPasswordFromServer(phone: phone)
    }
    
    func back() {
        self.viewPager.scrollToPage(index: 1)
    }
}

extension LoginisationViewController : NewPasswordViewDelegate {
    func save() {
        self.newPassword(phone: self.phone, code: self.newPasswordSwipeView!.codeTextField.text!, newPassword: self.newPasswordSwipeView!.newPasswordTextField.text!)
    }
}

extension LoginisationViewController {
    func auth (phone : String, password : String) {
        self.start()
        RequestManager.shared.auth(phone: phone.filterPhone(), password: password, success: { (responseObject) in
            User.remove()
            self.saveUser(responseObject: responseObject)
            self.performSegue(withIdentifier: LOGIN_ID, sender: self)
            self.stop()
            self.cleareDeviceId()
            self.setFCM()
        }) { (errorCode) in
            self.stop()
        }
    }
    
    private func cleareDeviceId () {
        if let deviceId = UserDefaults.standard.string(forKey: "deviceId") {
            RequestManager.shared.clearDeviceId(deviceId: deviceId, success: { (_) in
                
            }) { (_) in
                
            }
        }
    }
    
    private func setFCM () {
        if let token = Messaging.messaging().fcmToken {
            RequestManager.shared.setFCM(fcmToken: token, success: { (responseObject) in
                print("\(responseObject)")
            }) { (_) in
                
            }
        }
    }
    
    private func saveUser (responseObject: Dictionary<String, Any>) {
        let user = User()
        user.get(response: responseObject)
        user.save()
        user.isAuth(auth: true)
    }
    
    func getPasswordFromServer (phone : String) {
//        self.viewPager.scrollToPage(index: 3)
        self.start()
        self.phone = phone
        RequestManager.shared.forgotPassword(phone: phone.filterPhone(), success: { (responseObject) in
            self.viewPager.scrollToPage(index: 3)
            self.stop()
        }) { (errorCode) in
            self.stop()
        }
    }
    
    func newPassword (phone : String, code : String, newPassword : String) {
//        self.viewPager.scrollToPage(index: 1)
        self.start()
        RequestManager.shared.newPassword(phone: phone.filterPhone(), code: code, newPassword: newPassword, success: { (responseObject) in
            self.viewPager.scrollToPage(index: 1)
            self.stop()
        }) { (errorCode) in
            self.stop()
        }
    }
}
