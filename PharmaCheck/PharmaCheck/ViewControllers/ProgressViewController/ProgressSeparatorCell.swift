//
//  ProgressSeparatorCell.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ProgressSeparatorCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
}
