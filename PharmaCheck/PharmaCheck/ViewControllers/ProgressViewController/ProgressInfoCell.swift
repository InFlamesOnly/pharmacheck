//
//  ProgressInfoCell.swift
//  Remedy
//
//  Created by macOS on 07.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ProgressInfoCell: UITableViewCell {
    
    @IBOutlet weak var bckView : UIView!
    @IBOutlet weak var progressDate : UILabel!
    @IBOutlet weak var progressInfo : UILabel!
    @IBOutlet weak var progressName : UILabel!
    @IBOutlet weak var progressStatusLabel : UILabel!
    @IBOutlet weak var progressStatus : UIView!

    func configure (worksheet : Worksheets) {
        self.progressStatus.roundView()
        self.bckView.roundCorners(value: 6)
        self.bckView.addShadow()
        self.cellFromInfo(worksheet: worksheet)
        self.selectionStyle = .none
    }
    
    func cellFromInfo (worksheet : Worksheets) {
        self.progressDate.text = worksheet.date
        self.progressName.text = worksheet.name
        self.configStatus(worksheet: worksheet)
    }
    
    func configStatus (worksheet : Worksheets) {
        switch worksheet.status {
        case 0:
            self.lowStatus(worksheet: worksheet)
            break
        case 1:
            self.mediumStatus(worksheet: worksheet)
            break
        case 2:
            self.hightStatus(worksheet: worksheet)
            break
        default:
            break
        }
    }
    
    func lowStatus (worksheet : Worksheets) {
        self.progressStatusLabel.text = "Заявка подтверждена"
        self.progressStatus.backgroundColor = GREEN_COLOR
        self.progressInfo.isHidden = true
    }
    
    func mediumStatus (worksheet : Worksheets) {
        self.progressStatusLabel.text = "Заявка в обработке"
        self.progressStatus.backgroundColor = YELLOW_COLOR
        self.progressInfo.isHidden = false
        self.progressInfo.textColor = YELLOW_COLOR
        self.progressInfo.text = worksheet.text
    }
    
    func hightStatus (worksheet : Worksheets) {
        self.progressStatusLabel.text = "Заявка отклонена"
        self.progressStatus.backgroundColor = RED_COLOR
        self.progressInfo.isHidden = false
        self.progressInfo.textColor = RED_COLOR
        self.progressInfo.text = worksheet.text
    }

}
