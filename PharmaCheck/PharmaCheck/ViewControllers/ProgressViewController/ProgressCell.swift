//
//  ProgressCell.swift
//  Remedy
//
//  Created by macOS on 25.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ProgressCell: UITableViewCell {
    
    @IBOutlet weak var progress : UILabel!

    func configure (text : String) {
        self.setMultiplyColors(text: text)
        self.selectionStyle = .none
    }
    
    private func setMultiplyColors (text : String) {
        if let number = Int(text.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            let numberText = "\(number)"
            self.progress.attributedText = self.createAttributedString(fullString: text, subStringOne: "анкет", subStringTwo: numberText, color: VIOLET_COLOR)
        }
    }
    
    private func createAttributedString(fullString: String, subStringOne: String, subStringTwo: String, color: UIColor) -> NSMutableAttributedString {
        let range = (fullString as NSString).range(of: subStringOne)
        let range2 = (fullString as NSString).range(of: subStringTwo)
        let attributedString = NSMutableAttributedString(string:fullString)
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location: 0, length: fullString.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range2)
        
        return attributedString
    }
}
