//
//  SupportViewController.swift
//  Remedy
//
//  Created by macOS on 28.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import MessageUI

class SupportViewController: ActivityViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var firstPhone : UIButton!
    @IBOutlet weak var emailLabel : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(SupportViewController.email))
        emailLabel.isUserInteractionEnabled = true
        emailLabel.addGestureRecognizer(tap)
        //pharma.checkin@gmail.com
    }
    
    @IBAction func tapFirstPhone(_ sender: Any) {
        self.call(phone: (self.firstPhone.titleLabel!.text!.filterPhone()))
    }
    
    func call (phone : String) {
        let url = URL(string: "tel://\(phone)")
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    @objc func email(sender:UITapGestureRecognizer) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["pharma.checkin@gmail.com"])
            mail.setSubject("Письмо в службу поддержки")
            mail.setMessageBody("", isHTML: true)
            present(mail, animated: true)
        } else {
            print("Application is not able to send an email")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
