//
//  GradientButton.swift
//  Remedy
//
//  Created by macOS on 23.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class CornerRoundsButton: UIButton {

    let gradientLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        defaultInitializer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultInitializer()
    }
    
    
    func defaultInitializer() {
        setTitleColor(UIColor.white, for: .normal)
        round()
        backgroundColor = UIColor(red: 35/255.0, green: 64/255.0, blue: 142/255.0, alpha: 1.0)
    }
    
    private func round () {
        layer.cornerRadius = 6
        layer.masksToBounds = true
    }
}
